# HiRecyclerAdapter
* [![](https://www.jitpack.io/v/com.gitee.hijunmeng/hi-recycler-adapter.svg)](https://www.jitpack.io/#com.gitee.hijunmeng/hi-recycler-adapter)
* 使用RecylerView的Adapter会发现出现大量重复的样本代码，既增加了代码量，又会干扰主业务逻辑
* 因此此库旨在为用户提供一个非常便捷简单的使用Adapter的方式，让用户更专注与业务逻辑的开发

## 如何引用此库
* 本工程通过[JitPack | Publish JVM and Android libraries](https://www.jitpack.io/)进行发布
* 引用此库详见[https://www.jitpack.io/#com.gitee.hijunmeng/hi-recycler-adapter](https://www.jitpack.io/#com.gitee.hijunmeng/hi-recycler-adapter)
* Step 1. Add the JitPack repository to your root build.gradle at the end of repositories
```
	allprojects {
		repositories {
			...
			maven { url 'https://www.jitpack.io' }
		}
	}
```
* Step 2. Add the dependency
```
	dependencies {
	        implementation 'com.gitee.hijunmeng:hi-recycler-adapter:Tag'
	}
```

## 用法示例
* 以下以最通用的多类型的进行示例（DataBinding和ViewBinding类推）,具体用法可以参考工程中的demo
```java
//第一步，编写BindView(有多少种类型就写多少个BindView)
//注意BindView所指定的泛型即为列表项数据的具体子项的类型
public class BindView1 extends BaseCommonBindView<BindView1.Item>{
    @Override
    public int getItemLayoutResId(Item item) {
        return R.layout.item_one;//在此处返回每种类型对应的布局
    }

    @Override
    public void bindViewDataWithFull(RecyclerViewHolder holder, int position, Item item) {
        //将数据设置给相应的视图
        holder.setText(R.id.text, item.text);
    }
}
public class BindView2 extends BaseCommonBindView<BindView2.Item>{
    ...
}

//第二步，创建BaseCommonRecyclerAdapter后对上面的BindView进行注册即可
BaseCommonRecyclerAdapter adapter = new BaseCommonRecyclerAdapter(...);
adapter.register(new BindView1());
adapter.register(new BindView2());

//第三步，将adapter设置给RecyclerView
recyclerView.setAdapter(adapter);

//第四步，提交数据
List datas=new ArrayList();
datas.add(new BindView1.Item());
datas.add(new BindView2.Item());
datas.add(new BindView2.Item());
datas.add(new BindView1.Item());
adapter.submitList(datas);
```

## 版本说明
### 1.0.x
* 对RecyclerView Adapter进行抽象封装，使其使用起来更加简单
* 提供了item的单击和长按事件监听
* 提供了可以屏蔽item内部子view点击事件的item监听器，支持单击，双击，长按
* 提供对DataBinding和ViewBinding的支持
* 提供一个可以屏蔽item子view点击事件的监听器
* 提供一个可以按顺序添加的ItemDecoration，同时支持使用xml布局文件进行装饰绘制
* 提供了在网格布局中子view居中的计算方案
* 支持ConcatAdapter（recyclerview:1.2.0新增）
* 增加v2接口以满足androidx.recyclerview.widget.ListAdapter中AsyncListDiffer（1.0.1版本新增）
* 增加滑到顶部或底部监听RecyclerSlideHelper，以及提供forceSubmitList接口用于强制刷新UI（1.0.2版本新增）
* 优化在网格布局中子view居中的计算方案并提供demo(例如emoji表情面板)（1.0.3版本新增）
* 提供在底部或顶部的判断方法，以及滑到底或顶等方法的工具类RecyclerViewUtil（1.0.3版本新增）
* 增加了ViewHolder中获得RecyclerView的方法（1.0.4版本新增）
* 增加了BindView中获得自定义用户数据的方法（1.0.5版本新增）
* 增加了BindView中获得adapter对象的方法（1.0.5版本新增）

