# 保持自动生成的databinding类的inflate方法不被混淆
-keepclasseswithmembers class **.databinding.** {
    public static ** inflate(android.view.LayoutInflater,android.view.ViewGroup,boolean);
}