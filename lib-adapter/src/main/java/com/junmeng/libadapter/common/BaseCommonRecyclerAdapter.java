package com.junmeng.libadapter.common;


import com.junmeng.libadapter.base.CommonRecyclerAdapter;

public class BaseCommonRecyclerAdapter extends CommonRecyclerAdapter {

    public BaseCommonRecyclerAdapter() {
        super(new CommonBindViewManager());
    }

}
