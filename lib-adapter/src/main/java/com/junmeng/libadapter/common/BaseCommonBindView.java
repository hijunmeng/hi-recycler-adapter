package com.junmeng.libadapter.common;


import com.junmeng.libadapter.base.BaseBindView;

/**
 * 每种数据类型对应的BindView基类(通用)
 * 用户只需继承此类并实现抽象方法即可
 *
 * @param <T> 与视图对应的实体类
 */
public abstract class BaseCommonBindView<T> extends BaseBindView<T, RecyclerViewHolder> {

    /**
     * 获得布局资源id
     * @param item <T>的实例对象
     * @return
     */
    public abstract int getItemLayoutResId(T item);

}