package com.junmeng.libadapter.common;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.junmeng.libadapter.base.BaseRecyclerAdapter;

import java.util.List;

/**
 * 此类适用于单一数据类型的RecyclerView
 * 使用时只需继承此类，然后实现抽象方法即可
 * 如果是多数据类型的，请使用BaseCommonRecyclerAdapter
 *
 * @param <T> 与视图对应的实体类
 */
public abstract class BaseSingleRecyclerAdapter<T> extends BaseRecyclerAdapter<T, RecyclerViewHolder> {

    public BaseSingleRecyclerAdapter() {
    }

    /**
     * 在此处返回item布局id
     *
     * @return
     */
    public abstract int getItemLayoutResId();

    /**
     * 设置控件的显示内容
     *
     * @param holder
     * @param t
     */
    public abstract void onBindView(RecyclerViewHolder holder, int position, T t, @NonNull List<Object> payloads);

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder((RecyclerView) parent, LayoutInflater.from(parent.getContext()).inflate(getItemLayoutResId(), parent, false));
    }

    @Override
    public void onBindViewHolder(final @NonNull RecyclerViewHolder holder, int position, @NonNull List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
        onBindView(holder, position, getItem(position), payloads);
    }
}