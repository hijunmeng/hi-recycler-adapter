package com.junmeng.libadapter.viewbinding;

import androidx.viewbinding.ViewBinding;


import com.junmeng.libadapter.base.BaseBindView;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * 每种数据类型对应的BindView基类(适用于视图绑定)
 * 用户只需继承此类并实现抽象方法即可
 * 丧失了一个实体类对应多种布局的特性，只能一个实体类对应一种布局
 *
 * @param <T>  与视图对应的实体类
 * @param <VB> 视图绑定类
 */
public abstract class BaseViewBindingBindView<T, VB extends ViewBinding> extends BaseBindView<T, ViewBindingViewHolder<VB>> {
    public Type viewBindingType; //存放泛型VB的实际类型

    public BaseViewBindingBindView() {
        super();
        viewBindingType = getViewBindingType(getClass());
    }

    /**
     * 获得VB泛型的实际类型
     *
     * @param subclass 子类
     * @return
     */
    public Type getViewBindingType(Class<?> subclass) {
        Type superclass = subclass.getGenericSuperclass();
        if (superclass instanceof Class) {
            throw new RuntimeException("Missing type parameter.");
        }
        ParameterizedType parameterized = (ParameterizedType) superclass;
        return parameterized.getActualTypeArguments()[1];
    }

}