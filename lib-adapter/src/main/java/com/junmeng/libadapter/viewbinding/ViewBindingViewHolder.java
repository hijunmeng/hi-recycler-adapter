package com.junmeng.libadapter.viewbinding;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;

import java.lang.ref.WeakReference;

public class ViewBindingViewHolder<VB extends ViewBinding> extends RecyclerView.ViewHolder {

    public VB binding;
    private WeakReference<RecyclerView> mAttachRecyclerView;

    public ViewBindingViewHolder(VB binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public ViewBindingViewHolder(RecyclerView attachRecyclerView, VB binding) {
        this(binding);
        mAttachRecyclerView = new WeakReference<>(attachRecyclerView);
    }

    @Nullable
    public RecyclerView getAttachRecyclerView() {
        if (mAttachRecyclerView != null) {
            return mAttachRecyclerView.get();
        }
        return null;
    }
}