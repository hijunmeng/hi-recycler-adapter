package com.junmeng.libadapter.viewbinding;

import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;

import com.junmeng.libadapter.base.BaseBindView;
import com.junmeng.libadapter.base.BindViewStore;
import com.junmeng.libadapter.base.IBindViewManager;
import com.junmeng.libadapter.utils.BindViewKeyUtil;

import java.lang.reflect.Method;
import java.util.List;

/**
 * 相比较ViewBindingBindViewManager2，利用反射内部进行了绑定调用，因此BaseViewBindingBindView对用户来说更加简洁
 * 注意：
 * 请务必在混淆中增加如下已保证inflate方法不被混淆：
 * -keepclasseswithmembers class **.databinding.** {
 * public static ** inflate(**);
 * }
 *
 * @param <T>
 * @param <VB>
 */
public class ViewBindingBindViewManager<T, VB extends ViewBinding> implements IBindViewManager<T, ViewBindingViewHolder<VB>, BaseViewBindingBindView<T, VB>> {
    protected BindViewStore<Integer, BaseViewBindingBindView<T, VB>> bindViewStore = new BindViewStore<>();
    protected SparseArray<Class> viewTypeMapViewBindingClass = new SparseArray<>();

    @Override
    public ViewBindingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        Log.i("123456","viewBindingType:"+baseViewBindingBindView.viewBindingType);
        //利用反射进行bing操作
        VB result = null;
        Class cls = null;
        try {
            //由于以下使用到反射，因此反射方法必须不能被混淆
            cls = viewTypeMapViewBindingClass.get(viewType);
            Method bindMethod = cls.getMethod("inflate", LayoutInflater.class, ViewGroup.class, boolean.class);
            result = (VB) bindMethod.invoke(null, LayoutInflater.from(parent.getContext()), parent, false);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("发生错误：" + e.getMessage() + ",请确保填写的泛型（" + cls + "）无误或者在混淆中配置使得自动生成的databinding类的inflate方法不被混淆：\n-keepclasseswithmembers class **.databinding.** {\n" +
                    "    public static ** inflate(android.view.LayoutInflater,android.view.ViewGroup,boolean);\n" +
                    "}");
        }
        return new ViewBindingViewHolder((RecyclerView) parent, result);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewBindingViewHolder<VB> holder, int position, @NonNull T t, @NonNull List<Object> payloads) {
        bindViewStore.getBindView(getBindViewKey(t)).bindViewData(holder, position, t, payloads);
    }

    @Override
    public void onViewRecycled(@NonNull ViewBindingViewHolder<VB> holder, @NonNull T t) {
        BaseBindView bindView = bindViewStore.getBindView(getBindViewKey(t));
        if (bindView == null) {
            return;
        }
        bindView.onViewRecycled(holder);
    }

    @Override
    public void register(@NonNull BaseViewBindingBindView bindView) {
        bindViewStore.putBindView(BindViewKeyUtil.getKey(bindView.itemBeanType), bindView);
    }

    @Override
    public int getItemViewType(int position, @NonNull T t) {
        int viewType = getBindViewKey(t);
        BaseViewBindingBindView baseViewBindingBindView = bindViewStore.getBindView(getBindViewKey(t));
        if (baseViewBindingBindView == null) {
            throw new RuntimeException("Don't forget to register " + t.getClass() + " before using");
        }
        viewTypeMapViewBindingClass.put(viewType, (Class) baseViewBindingBindView.viewBindingType);
        return viewType;
    }

    private int getBindViewKey(@NonNull T t) {
        return BindViewKeyUtil.getKey(t.getClass());
    }
}