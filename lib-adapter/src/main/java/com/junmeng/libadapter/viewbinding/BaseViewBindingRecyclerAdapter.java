package com.junmeng.libadapter.viewbinding;


import com.junmeng.libadapter.base.CommonRecyclerAdapter;

/**
 * 注意：
 * 请务必在混淆中增加如下已保证inflate方法不被混淆：
 * -keepclasseswithmembers class **.databinding.** {
 * public static ** inflate(**);
 * }
 */
public class BaseViewBindingRecyclerAdapter extends CommonRecyclerAdapter {
    public BaseViewBindingRecyclerAdapter() {
        super(new ViewBindingBindViewManager());
    }
}
