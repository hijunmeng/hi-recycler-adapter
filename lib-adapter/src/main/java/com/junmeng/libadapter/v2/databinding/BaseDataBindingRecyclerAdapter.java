package com.junmeng.libadapter.v2.databinding;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncDifferConfig;
import androidx.recyclerview.widget.DiffUtil;

import com.junmeng.libadapter.databinding.DataBindingBindViewManager;
import com.junmeng.libadapter.v2.base.CommonRecyclerAdapter;

public class BaseDataBindingRecyclerAdapter extends CommonRecyclerAdapter {
    @Deprecated
    public BaseDataBindingRecyclerAdapter() {
        super(new DataBindingBindViewManager());
    }

    public BaseDataBindingRecyclerAdapter(@NonNull DiffUtil.ItemCallback diffCallback) {
        super(diffCallback, new DataBindingBindViewManager());
    }

    public BaseDataBindingRecyclerAdapter(@NonNull AsyncDifferConfig config) {
        super(config, new DataBindingBindViewManager());
    }
}
