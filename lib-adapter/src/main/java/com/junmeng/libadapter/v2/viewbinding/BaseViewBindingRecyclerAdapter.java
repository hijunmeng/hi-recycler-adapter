package com.junmeng.libadapter.v2.viewbinding;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncDifferConfig;
import androidx.recyclerview.widget.DiffUtil;

import com.junmeng.libadapter.v2.base.CommonRecyclerAdapter;
import com.junmeng.libadapter.viewbinding.ViewBindingBindViewManager;

/**
 * 注意：
 * 请务必在混淆中增加如下已保证inflate方法不被混淆：
 * -keepclasseswithmembers class **.databinding.** {
 * public static ** inflate(**);
 * }
 */
public class BaseViewBindingRecyclerAdapter extends CommonRecyclerAdapter {
    @Deprecated
    public BaseViewBindingRecyclerAdapter() {
        super(new ViewBindingBindViewManager());
    }

    public BaseViewBindingRecyclerAdapter(@NonNull DiffUtil.ItemCallback diffCallback) {
        super(diffCallback, new ViewBindingBindViewManager());
    }

    public BaseViewBindingRecyclerAdapter(@NonNull AsyncDifferConfig config) {
        super(config, new ViewBindingBindViewManager());
    }
}
