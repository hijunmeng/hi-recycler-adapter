package com.junmeng.libadapter.v2.common;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncDifferConfig;
import androidx.recyclerview.widget.DiffUtil;

import com.junmeng.libadapter.common.CommonBindViewManager;
import com.junmeng.libadapter.v2.base.CommonRecyclerAdapter;

public class BaseCommonRecyclerAdapter extends CommonRecyclerAdapter {
    @Deprecated//不推荐使用此构造函数
    public BaseCommonRecyclerAdapter() {
        super(new CommonBindViewManager());
    }

    public BaseCommonRecyclerAdapter(@NonNull DiffUtil.ItemCallback diffCallback) {
        super(diffCallback, new CommonBindViewManager());
    }

    public BaseCommonRecyclerAdapter(@NonNull AsyncDifferConfig config) {
        super(config, new CommonBindViewManager());
    }
}
