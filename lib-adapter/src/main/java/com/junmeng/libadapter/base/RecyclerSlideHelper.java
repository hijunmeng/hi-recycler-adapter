package com.junmeng.libadapter.base;

import android.view.GestureDetector;
import android.view.MotionEvent;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

/**
 * 滑动监听助手
 * 可以监听是否滑到顶部或底部
 */
public class RecyclerSlideHelper {
    private List<OnSlideListener> mListeners = new ArrayList<>();
    private RecyclerView mHost;

    private volatile boolean isSlideUp = false; //是否向上滑

    public RecyclerSlideHelper(@NonNull RecyclerView recyclerView) {
        mHost = recyclerView;
        init();
    }

    public void addOnSlideListener(@NonNull OnSlideListener listener) {
        mListeners.add(listener);
    }

    public void removeOnSlideListener(@NonNull OnSlideListener listener) {
        mListeners.remove(listener);
    }

    private void init() {
        GestureDetector gestureDetector = new GestureDetector(mHost.getContext(), new RecyclerItemClickListener.SimpleOnGestureListener() {
            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                isSlideUp = distanceY > 0; //distanceY大于0表示正向上滑动
                return false;
            }
        });

        mHost.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                //canScrollVertically(1)的值表示是否能向上滚动，true表示能滚动，false表示已经滚动到底部
                //canScrollVertically(-1)的值表示是否能向下滚动，true表示能滚动，false表示已经滚动到顶部
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if (isSlideUp && !mHost.canScrollVertically(1)) { //false表示已经滚动到底部
                        for (OnSlideListener listener : mListeners) {
                            listener.onSlideBottom(mHost);
                        }
                    }
                    if (!isSlideUp && !mHost.canScrollVertically(-1)) { //false表示已经滚动到顶部
                        for (OnSlideListener listener : mListeners) {
                            listener.onSlideTop(mHost);
                        }
                    }
                }
            }
        });
        mHost.addOnItemTouchListener(new RecyclerView.SimpleOnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(@NonNull RecyclerView rv, @NonNull MotionEvent e) {
                gestureDetector.onTouchEvent(e);
                return false;
            }
        });
    }


    public interface OnSlideListener {
        /**
         * 是否滑到底部
         *
         * @param recyclerView
         */
        void onSlideBottom(RecyclerView recyclerView);

        /**
         * 是否滑到顶部
         *
         * @param recyclerView
         */
        void onSlideTop(RecyclerView recyclerView);
    }

    public static class SimpleOnSlideListener implements OnSlideListener {
        @Override
        public void onSlideBottom(RecyclerView recyclerView) {

        }

        @Override
        public void onSlideTop(RecyclerView recyclerView) {

        }
    }
}