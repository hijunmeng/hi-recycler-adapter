package com.junmeng.libadapter.base;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

/**
 *
 * @param <T> 与视图对应的实体类
 * @param <VH> ViewHolder
 * @param <BV> BaseBindView
 */
public interface IBindViewManager<T, VH extends RecyclerView.ViewHolder, BV extends BaseBindView> {
    /**
     * 创建ViewHolder
     *
     * @param parent   实际上是RecyclerView对象
     * @param viewType
     * @return
     */
    VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType);

    /**
     * 在此实现item数据与ViewHolder的业务绑定
     *
     * @param holder
     * @param position
     * @param item     position处的item对象
     * @param payloads 用于局部刷新
     */
    void onBindViewHolder(@NonNull VH holder, int position, @NonNull T item, @NonNull List<Object> payloads);

    /**
     * 视图回收时回调此
     *
     * @param holder
     * @param item   holder所在position处的item对象
     */
    void onViewRecycled(@NonNull VH holder, @NonNull T item);

    /**
     * 注册BindView
     *
     * @param bindView
     */
    void register(@NonNull BV bindView);

    /**
     * 获得指定位置处的item的视图类型
     *
     * @param position
     * @param item     指定position处的item对象
     * @return
     */
    int getItemViewType(int position, @NonNull T item);
}
