package com.junmeng.libadapter.base;

import android.view.View;

public interface OnItemLongClickListener<T> {
    boolean onItemLongClick(View v, int position, T t);
}