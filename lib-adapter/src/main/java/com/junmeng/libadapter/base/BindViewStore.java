package com.junmeng.libadapter.base;

import androidx.annotation.NonNull;

import java.util.HashMap;

/**
 * BindView存储器
 *
 * @param <Key>
 * @param <Value>
 */
public class BindViewStore<Key, Value> {
    public HashMap<Key, Value> bindViewMap = new HashMap<>();

    public Value getBindView(Key type) {
        return bindViewMap.get(type);
    }

    public void putBindView(@NonNull Key type, @NonNull Value bindView) {
        bindViewMap.put(type, bindView);
    }

    public void clear() {
        bindViewMap.clear();
    }
}