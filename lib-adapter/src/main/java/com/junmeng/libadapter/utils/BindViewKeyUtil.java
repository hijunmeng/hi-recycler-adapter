package com.junmeng.libadapter.utils;

import java.lang.reflect.Type;

public class BindViewKeyUtil {
    /**
     * 获得类对象的唯一标识
     * @param cls
     * @return
     */
    public static int getKey(Class cls) {
        return  cls.hashCode();
    }


    /**
     * 获得类对象的唯一标识
     * @param type
     * @return
     */
    public static int getKey(Type type) {
        return getKey((Class) type);
    }
}
