package com.junmeng.libadapter.utils;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * RecyclerView常用功能
 */
public class RecyclerViewUtil {

    /**
     * 是否已经滑动到底部
     *
     * @param recyclerView
     * @return
     */
    public static boolean isScrollToBottom(@NonNull RecyclerView recyclerView) {
        return !recyclerView.canScrollVertically(1);
    }

    /**
     * 是否已经滑动到顶部
     *
     * @param recyclerView
     * @return
     */
    public static boolean isScrollToTop(@NonNull RecyclerView recyclerView) {
        return !recyclerView.canScrollVertically(-1);
    }

    /**
     * 平滑滑动到底部
     *
     * @param recyclerView
     * @return true--成功
     */
    public static boolean smoothScrollToBottom(@NonNull RecyclerView recyclerView) {
        return scrollToBottom(recyclerView, true);
    }

    /**
     * 平滑滑动到顶部
     *
     * @param recyclerView
     * @return true--成功
     */
    public static boolean smoothScrollToTop(@NonNull RecyclerView recyclerView) {
        return scrollToTop(recyclerView, true);
    }

    /**
     * 滑动到顶部
     *
     * @param recyclerView
     * @param smooth       是否平滑滑动
     * @return true--成功
     */
    public static boolean scrollToTop(@NonNull RecyclerView recyclerView, boolean smooth) {
        if (recyclerView == null || recyclerView.getAdapter() == null) {
            return false;
        }
        int count = recyclerView.getAdapter().getItemCount();
        if (count > 0) {
            if (smooth) {
                recyclerView.smoothScrollToPosition(0);
            } else {
                recyclerView.scrollToPosition(0);
            }
            return true;
        }
        return false;
    }

    /**
     * 滑动到底部
     *
     * @param recyclerView
     * @param smooth       是否平滑滑动
     * @return true--成功
     */
    public static boolean scrollToBottom(@NonNull RecyclerView recyclerView, boolean smooth) {
        if (recyclerView == null || recyclerView.getAdapter() == null) {
            return false;
        }
        int position = recyclerView.getAdapter().getItemCount() - 1;
        if (position != -1) {
            if (smooth) {
                recyclerView.smoothScrollToPosition(position);
            } else {
                recyclerView.scrollToPosition(position);
            }
            return true;
        }
        return false;
    }
}