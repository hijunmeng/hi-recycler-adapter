package com.junmeng.libadapter.databinding;


import com.junmeng.libadapter.base.CommonRecyclerAdapter;

public class BaseDataBindingRecyclerAdapter extends CommonRecyclerAdapter {
    public BaseDataBindingRecyclerAdapter() {
        super(new DataBindingBindViewManager());
    }
}
