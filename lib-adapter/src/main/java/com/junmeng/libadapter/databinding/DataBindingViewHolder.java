package com.junmeng.libadapter.databinding;

import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.ref.WeakReference;

public class DataBindingViewHolder<VDB extends ViewDataBinding> extends RecyclerView.ViewHolder {

    public VDB binding;
    private WeakReference<RecyclerView> mAttachRecyclerView;

    public DataBindingViewHolder(VDB binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public DataBindingViewHolder(RecyclerView attachRecyclerView, VDB binding) {
        this(binding);
        mAttachRecyclerView = new WeakReference<>(attachRecyclerView);
    }

    @Nullable
    public RecyclerView getAttachRecyclerView() {
        if (mAttachRecyclerView != null) {
            return mAttachRecyclerView.get();
        }
        return null;
    }
}