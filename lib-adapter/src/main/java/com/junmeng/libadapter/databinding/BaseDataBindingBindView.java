package com.junmeng.libadapter.databinding;

import androidx.databinding.ViewDataBinding;

import com.junmeng.libadapter.base.BaseBindView;


/**
 * 每种数据类型对应的BindView基类(适用于数据绑定)
 * 用户只需继承此类并实现抽象方法即可
 *
 * @param <T>   与视图对应的实体类
 * @param <VDB> 数据绑定类
 */
public abstract class BaseDataBindingBindView<T, VDB extends ViewDataBinding> extends BaseBindView<T, DataBindingViewHolder<VDB>> {
    /**
     * 获得布局资源id
     * @param item <T>的实例对象
     * @return
     */
    public abstract int getItemLayoutResId(T item);

}