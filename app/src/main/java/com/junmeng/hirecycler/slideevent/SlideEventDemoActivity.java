package com.junmeng.hirecycler.slideevent;

import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.junmeng.hirecycler.databinding.ActivitySlideEventDemoBinding;
import com.junmeng.hirecycler.databinding.ItemSlideTestBinding;
import com.junmeng.libadapter.base.RecyclerSlideHelper;
import com.junmeng.libadapter.v2.viewbinding.BaseSingleViewBindingRecyclerAdapter;
import com.junmeng.libadapter.viewbinding.ViewBindingViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * 演示滑到底部或顶部
 */
public class SlideEventDemoActivity extends AppCompatActivity {

    private ActivitySlideEventDemoBinding mBinding;
    private MyAdapter mListAdapter;
    private ArrayList<String> mListItems = new ArrayList<>();
    private MyAdapter mGridAdapter;
    private ArrayList<String> mGridItems = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = ActivitySlideEventDemoBinding.inflate(getLayoutInflater());
        setContentView(mBinding.getRoot());

        initListView();
        initGridView();
    }

    private void initListView() {
        mListAdapter = new MyAdapter();
        mBinding.recyclerView.setAdapter(mListAdapter);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(this));

        new RecyclerSlideHelper(mBinding.recyclerView).addOnSlideListener(new RecyclerSlideHelper.OnSlideListener() {
            @Override
            public void onSlideBottom(RecyclerView recyclerView) {
                for (int i = 0; i < 5; i++) {
                    mListItems.add("load next " + i);
                }
                mListAdapter.forceSubmitList(mListItems);
            }

            @Override
            public void onSlideTop(RecyclerView recyclerView) {
                for (int i = 0; i < 3; i++) {
                    mListItems.add(0, "load previous " + i);
                }
                mListAdapter.forceSubmitList(mListItems);
            }
        });
    }

    private void initGridView() {
        mGridAdapter = new MyAdapter();
        mBinding.gridView.setAdapter(mGridAdapter);
        mBinding.gridView.setLayoutManager(new GridLayoutManager(this, 3));

        new RecyclerSlideHelper(mBinding.gridView).addOnSlideListener(new RecyclerSlideHelper.OnSlideListener() {
            @Override
            public void onSlideBottom(RecyclerView recyclerView) {
                for (int i = 0; i < 5; i++) {
                    mGridItems.add("load next " + i);
                }
                mGridAdapter.forceSubmitList(mGridItems);
            }

            @Override
            public void onSlideTop(RecyclerView recyclerView) {
                for (int i = 0; i < 3; i++) {
                    mGridItems.add(0, "load previous " + i);
                }
                mGridAdapter.forceSubmitList(mGridItems);
            }
        });
    }

    private void showToast(String text) {
        runOnUiThread(() -> {
            Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
        });

    }


    class MyAdapter extends BaseSingleViewBindingRecyclerAdapter<String, ItemSlideTestBinding> {

        public MyAdapter() {
            super(new DiffUtil.ItemCallback<String>() {

                @Override
                public boolean areItemsTheSame(@NonNull String oldItem, @NonNull String newItem) {
                    return oldItem.equals(newItem);
                }

                @Override
                public boolean areContentsTheSame(@NonNull String oldItem, @NonNull String newItem) {
                    return oldItem.equals(newItem);
                }
            });
        }

        @Override
        public void onBindView(@NonNull ViewBindingViewHolder<ItemSlideTestBinding> holder, int position, String item, @NonNull List<Object> payloads) {
            holder.binding.text.setText("position=" + position + ":" + item);
        }
    }
}