package com.junmeng.hirecycler;

public class TestBean {

    public String s;

    public TestBean(String s) {
        this.s = s;
    }

    @Override
    public String toString() {
        return "TestBean{" +
                "s='" + s + '\'' +
                '}';
    }
}
