package com.junmeng.hirecycler;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.junmeng.hirecycler.clickevent.ClickEventDemoActivity;
import com.junmeng.hirecycler.common.CommonDemoActivity;
import com.junmeng.hirecycler.common.CommonDemoV2Activity;
import com.junmeng.hirecycler.databinding.DatabindingDemoActivity;
import com.junmeng.hirecycler.databinding.DatabindingDemoV2Activity;
import com.junmeng.hirecycler.itemdecoration.ItemDecorationEnterActivity;
import com.junmeng.hirecycler.slideevent.SlideEventDemoActivity;
import com.junmeng.hirecycler.viewbinding.ViewbindingDemoActivity;
import com.junmeng.hirecycler.viewbinding.ViewbindingDemoV2Activity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    void gotoActivity(Class cls) {
        Intent intent = new Intent(this, cls);
        startActivity(intent);
    }

    public void onClickCommon(View view) {
        gotoActivity(CommonDemoActivity.class);
    }

    public void onClickDataBinding(View view) {
        gotoActivity(DatabindingDemoActivity.class);
    }

    public void onClickViewBinding(View view) {
        gotoActivity(ViewbindingDemoActivity.class);
    }

    public void onClickItemClick(View view) {
        gotoActivity(ClickEventDemoActivity.class);
    }

    public void onClickItemDecoration(View view) {
        gotoActivity(ItemDecorationEnterActivity.class);
    }

    public void onClickCommonV2(View view) {
        gotoActivity(CommonDemoV2Activity.class);
    }

    public void onClickDataBindingV2(View view) {
        gotoActivity(DatabindingDemoV2Activity.class);
    }

    public void onClickViewBindingV2(View view) {
        gotoActivity(ViewbindingDemoV2Activity.class);
    }

    public void onClickSlideEvent(View view) {
        gotoActivity(SlideEventDemoActivity.class);
    }
}