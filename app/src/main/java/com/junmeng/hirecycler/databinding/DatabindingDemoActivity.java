package com.junmeng.hirecycler.databinding;

import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.junmeng.hirecycler.TestBean;
import com.junmeng.libadapter.databinding.BaseDataBindingRecyclerAdapter;

import java.util.Random;

public class DatabindingDemoActivity extends AppCompatActivity {
    ActivityDatabindingDemoBinding mBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding=ActivityDatabindingDemoBinding.inflate(getLayoutInflater());
        setContentView(mBinding.getRoot());

        BaseDataBindingRecyclerAdapter adapter = new BaseDataBindingRecyclerAdapter();
        adapter.setOnItemClickLitener((v, position, o) -> {
            Toast.makeText(this, "position=" + position, Toast.LENGTH_SHORT).show();
        });
        mBinding.recycler.setLayoutManager(new LinearLayoutManager(this));
        mBinding.recycler.setAdapter(adapter);
        mBinding.recycler.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        adapter.register(new BindView1());
        adapter.register(new BindView2());
        adapter.register(new BindView3());

        Random r=new Random();
        for(int i=0;i<50;i++){
            switch(r.nextInt(3)){
                case 0:
                    adapter.addItem(new BindView1.Item("i"+i));
                    break;
                case 1:
                    adapter.addItem(new BindView2.Item("i"+i, android.R.drawable.sym_def_app_icon));
                    break;
                case 2:
                    adapter.addItem(new BindView3.Item("i"+i, android.R.mipmap.sym_def_app_icon));
                    break;
            }
        }

    }
}