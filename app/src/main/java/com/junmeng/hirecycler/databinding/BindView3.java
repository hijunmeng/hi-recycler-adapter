package com.junmeng.hirecycler.databinding;

import android.widget.Toast;

import com.junmeng.hirecycler.R;
import com.junmeng.hirecycler.TestBean;
import com.junmeng.libadapter.databinding.BaseDataBindingBindView;
import com.junmeng.libadapter.databinding.DataBindingViewHolder;

public class BindView3 extends BaseDataBindingBindView<BindView3.Item, ItemDatabindingThreeBinding> {

    @Override
    public int getItemLayoutResId(Item item) {
        return R.layout.item_databinding_three;
    }

    @Override
    public void bindViewDataWithFull(DataBindingViewHolder<ItemDatabindingThreeBinding> holder, int position, BindView3.Item item) {
        TestBean tb=getUserData();
        holder.binding.text.setText(item.text+",UserData="+tb+",adapter="+getAdapter());
        holder.binding.image.setImageResource(item.imageResId);
        holder.binding.button.setOnClickListener((v) -> {
            Toast.makeText(holder.itemView.getContext(), "click", Toast.LENGTH_SHORT).show();
        });
    }


    public static class Item {
        public String text;
        public int imageResId;

        public Item(String text, int imageResId) {
            this.text = text;
            this.imageResId = imageResId;
        }
    }
}
