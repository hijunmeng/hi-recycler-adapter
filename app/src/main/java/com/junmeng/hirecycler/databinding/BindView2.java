package com.junmeng.hirecycler.databinding;

import com.junmeng.hirecycler.R;
import com.junmeng.hirecycler.TestBean;
import com.junmeng.libadapter.databinding.BaseDataBindingBindView;
import com.junmeng.libadapter.databinding.DataBindingViewHolder;

public class BindView2 extends BaseDataBindingBindView<BindView2.Item, ItemDatabindingTwoBinding> {

    @Override
    public void bindViewDataWithFull(DataBindingViewHolder<ItemDatabindingTwoBinding> holder, int position, Item item) {
        TestBean tb=getUserData();
        holder.binding.text.setText(item.text+",UserData="+tb+",adapter="+getAdapter());
        holder.binding.image.setImageResource(item.imageResId);
    }

    @Override
    public int getItemLayoutResId(Item item) {
        return R.layout.item_databinding_two;
    }

    public static class Item {
        public String text;
        public int imageResId;

        public Item(String text, int imageResId) {
            this.text = text;
            this.imageResId = imageResId;
        }

    }
}
