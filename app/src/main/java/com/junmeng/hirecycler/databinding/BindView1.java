package com.junmeng.hirecycler.databinding;

import com.junmeng.hirecycler.R;
import com.junmeng.hirecycler.TestBean;
import com.junmeng.libadapter.databinding.BaseDataBindingBindView;
import com.junmeng.libadapter.databinding.DataBindingViewHolder;

public class BindView1 extends BaseDataBindingBindView<BindView1.Item, ItemDatabindingOneBinding> {

    @Override
    public void bindViewDataWithFull(DataBindingViewHolder<ItemDatabindingOneBinding> holder, int position, Item item) {
        TestBean tb=getUserData();
        holder.binding.text.setText(item.text+",UserData="+tb+",adapter="+getAdapter());
    }

    @Override
    public int getItemLayoutResId(Item item) {
        return R.layout.item_databinding_one;
    }

    public static class Item {
        public Item(String text) {
            this.text = text;
        }

        public String text;

    }
}
