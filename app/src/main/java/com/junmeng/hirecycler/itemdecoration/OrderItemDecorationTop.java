package com.junmeng.hirecycler.itemdecoration;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.junmeng.hirecycler.R;
import com.junmeng.libadapter.decoration.BaseLayoutOrderItemDecoration;


public class OrderItemDecorationTop extends BaseLayoutOrderItemDecoration {
    @Override
    public int getItemDecorationLayoutResId() {
        return R.layout.item_decoration_top;
    }

    @Override
    public void initItemDecorationLayoutViews(@NonNull View itemView) {

    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        outRect.top=getItemDecorationHeight();
    }

    @Override
    public void onDraw(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        int count = parent.getChildCount();
        for (int i = 0; i < count; i++) {
            View child = parent.getChildAt(i);
            setCurrentItemDecorationRestMeasureWidth(getCurrentOrderDecoratedMeasuredWidth(child,parent,state));
            drawDecorationViewOnLeftOrTop(c, child, parent, state);
        }
    }
}