package com.junmeng.hirecycler.itemdecoration;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.junmeng.hirecycler.R;
import com.junmeng.hirecycler.databinding.ActivityOrderItemDecorationDemoBinding;
import com.junmeng.hirecycler.databinding.ItemSimpleDecorationBinding;
import com.junmeng.libadapter.viewbinding.BaseSingleViewBindingRecyclerAdapter;
import com.junmeng.libadapter.viewbinding.ViewBindingViewHolder;

import java.util.List;

/**
 * 演示了具有排序功能和可以使用xml布局进行绘制的BaseLayoutOrderItemDecoration的功能
 */
public class OrderItemDecorationDemoActivity extends AppCompatActivity {

    ActivityOrderItemDecorationDemoBinding binding;
    MyAdapter myAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=ActivityOrderItemDecorationDemoBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initRecyclerView();
        initData();
    }

    private void initData() {
        myAdapter.addItem(new ItemData());
        myAdapter.addItem(new ItemData());
        myAdapter.addItem(new ItemData());
        myAdapter.addItem(new ItemData());
        myAdapter.addItem(new ItemData());
    }
    void initRecyclerView() {
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //BaseLayoutOrderItemDecoration可以看到并不会被覆盖，而是按照顺序依次绘制
        binding.recyclerView.addItemDecoration(new OrderItemDecorationEnd());
        binding.recyclerView.addItemDecoration(new OrderItemDecorationStart());
        binding.recyclerView.addItemDecoration(new OrderItemDecorationTop());
        binding.recyclerView.addItemDecoration(new OrderItemDecorationBottom());

        binding.recyclerView.addItemDecoration(new OrderItemDecorationTop());
        binding.recyclerView.addItemDecoration(new OrderItemDecorationBottom());
        binding.recyclerView.addItemDecoration(new OrderItemDecorationStart());
        binding.recyclerView.addItemDecoration(new OrderItemDecorationEnd());

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, RecyclerView.VERTICAL);//两个DividerItemDecoration的话可以看到会覆盖
        dividerItemDecoration.setDrawable(getDrawable(R.drawable.shape_item_decoration_inset));
//        dividerItemDecoration.setDrawable(getDrawable(R.drawable.shape_item_decoration));
        binding.recyclerView.addItemDecoration(dividerItemDecoration);
        binding.recyclerView.setAdapter(myAdapter = new MyAdapter());
    }


    class MyAdapter extends BaseSingleViewBindingRecyclerAdapter<ItemData, ItemSimpleDecorationBinding> {

        @Override
        public void onBindView(@NonNull ViewBindingViewHolder<ItemSimpleDecorationBinding> holder, int position, ItemData itemData, @NonNull List<Object> payloads) {
            holder.binding.text.setText("i am ItemView");
        }
    }

    class ItemData {

    }
}