package com.junmeng.hirecycler.itemdecoration;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.junmeng.hirecycler.R;
import com.junmeng.libadapter.decoration.BaseLayoutOrderItemDecoration;

public class OrderItemDecorationEnd extends BaseLayoutOrderItemDecoration {
    @Override
    public int getItemDecorationLayoutResId() {
        return R.layout.item_decoration_end;
    }

    @Override
    public void initItemDecorationLayoutViews(@NonNull View itemView) {

    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        outRect.right = getItemDecorationWidth();

    }

    @Override
    public void onDraw(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        RecyclerView.LayoutManager layoutManager = parent.getLayoutManager();
        int count = parent.getChildCount();
        for (int i = 0; i < count; i++) {
            View child = parent.getChildAt(i);
            setCurrentItemDecorationRestMeasureHeight(getCurrentOrderDecoratedMeasuredHeight(child, parent, state));
            drawDecorationViewOnRight(c, child, parent, state);
        }
    }
}
