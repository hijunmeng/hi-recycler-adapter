package com.junmeng.hirecycler.itemdecoration;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.junmeng.hirecycler.R;

public class ItemDecorationEnterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_decoration_enter);
    }

    void gotoActivity(Class cls) {
        Intent intent = new Intent(this, cls);
        startActivity(intent);
    }

    public void onClickOrder(View view) {
        gotoActivity(OrderItemDecorationDemoActivity.class);
    }

    public void onClickGridWrap(View view) {
        gotoActivity(GridWrapItemDecorationDemoActivity.class);
    }
}