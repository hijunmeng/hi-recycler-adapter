package com.junmeng.hirecycler.itemdecoration;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;

import com.junmeng.hirecycler.databinding.ActivityGridWrapItemDecorationDemoBinding;
import com.junmeng.hirecycler.databinding.ItemGridWrapItemdecoration2Binding;
import com.junmeng.hirecycler.databinding.ItemGridWrapItemdecorationBinding;
import com.junmeng.libadapter.decoration.GridWrapItemDecoration;
import com.junmeng.libadapter.v2.viewbinding.BaseSingleViewBindingRecyclerAdapter;
import com.junmeng.libadapter.viewbinding.ViewBindingViewHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GridWrapItemDecorationDemoActivity extends AppCompatActivity {
    ActivityGridWrapItemDecorationDemoBinding binding;
    BaseSingleViewBindingRecyclerAdapter adapter;
    BaseSingleViewBindingRecyclerAdapter adapter2;
    List<Object> items = new ArrayList<>();

    int spanCount = 7;
    int verticalSpace = 20;
    int itemCount = 16;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityGridWrapItemDecorationDemoBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.btnConfirm.setOnClickListener(v -> {
           int itemWidth=Integer.parseInt(binding.etItemWidth.getText().toString()) ;
           int spanCount=Integer.parseInt(binding.etSpanCount.getText().toString()) ;
           int verticalSpace=Integer.parseInt(binding.etVerticalSpace.getText().toString()) ;
            ((GridWrapItemDecoration) binding.recyclerView1.getItemDecorationAt(0)).setItemWidth(binding.recyclerView1,itemWidth);
            ((GridWrapItemDecoration) binding.recyclerView1.getItemDecorationAt(0)).setSpanCount(binding.recyclerView1,spanCount);
            ((GridLayoutManager) binding.recyclerView1.getLayoutManager()).setSpanCount(spanCount);
            ((GridWrapItemDecoration) binding.recyclerView1.getItemDecorationAt(0)).setVerticalSpace(binding.recyclerView1,verticalSpace);

            ((GridWrapItemDecoration) binding.recyclerView2.getItemDecorationAt(0)).setItemWidth(binding.recyclerView2,itemWidth);
            ((GridWrapItemDecoration) binding.recyclerView2.getItemDecorationAt(0)).setSpanCount(binding.recyclerView2,spanCount);
            ((GridLayoutManager) binding.recyclerView2.getLayoutManager()).setSpanCount(spanCount);
            ((GridWrapItemDecoration) binding.recyclerView2.getItemDecorationAt(0)).setVerticalSpace(binding.recyclerView2,verticalSpace);

            ((GridWrapItemDecoration) binding.recyclerView21.getItemDecorationAt(0)).setItemWidth(binding.recyclerView21,itemWidth);
            ((GridWrapItemDecoration) binding.recyclerView21.getItemDecorationAt(0)).setSpanCount(binding.recyclerView21,spanCount);
            ((GridLayoutManager) binding.recyclerView21.getLayoutManager()).setSpanCount(spanCount);
            ((GridWrapItemDecoration) binding.recyclerView21.getItemDecorationAt(0)).setVerticalSpace(binding.recyclerView21,verticalSpace);

            ((GridWrapItemDecoration) binding.recyclerView22.getItemDecorationAt(0)).setItemWidth(binding.recyclerView22,itemWidth);
            ((GridWrapItemDecoration) binding.recyclerView22.getItemDecorationAt(0)).setSpanCount(binding.recyclerView22,spanCount);
            ((GridLayoutManager) binding.recyclerView22.getLayoutManager()).setSpanCount(spanCount);
            ((GridWrapItemDecoration) binding.recyclerView22.getItemDecorationAt(0)).setVerticalSpace(binding.recyclerView22,verticalSpace);
        });


        adapter = new BaseSingleViewBindingRecyclerAdapter<Object, ItemGridWrapItemdecorationBinding>() {
            @Override
            public void onBindView(@NonNull ViewBindingViewHolder<ItemGridWrapItemdecorationBinding> holder, int position, Object o, @NonNull List<Object> payloads) {
            holder.binding.text.setText(""+position);
            }
        };
        adapter2 = new BaseSingleViewBindingRecyclerAdapter<Object, ItemGridWrapItemdecoration2Binding>() {
            @Override
            public void onBindView(@NonNull ViewBindingViewHolder<ItemGridWrapItemdecoration2Binding> holder, int position, Object o, @NonNull List<Object> payloads) {
            }
        };

        //SPREAD_INSIDE效果
        binding.recyclerView1.setLayoutManager(new GridLayoutManager(this, spanCount));
        binding.recyclerView1.setAdapter(adapter);
        binding.recyclerView1.addItemDecoration(new GridWrapItemDecoration(spanCount, verticalSpace));

        binding.recyclerView2.setLayoutManager(new GridLayoutManager(this, spanCount));
        binding.recyclerView2.setAdapter(adapter);
        binding.recyclerView2.addItemDecoration(new GridWrapItemDecoration(spanCount, verticalSpace));


        //SPREAD效果
        binding.recyclerView21.setLayoutManager(new GridLayoutManager(this, spanCount));
        binding.recyclerView21.setAdapter(adapter2);
        binding.recyclerView21.addItemDecoration(new GridWrapItemDecoration(GridWrapItemDecoration.UIMode.SPREAD, spanCount, verticalSpace));

        binding.recyclerView22.setLayoutManager(new GridLayoutManager(this, spanCount));
        binding.recyclerView22.setAdapter(adapter2);
        binding.recyclerView22.addItemDecoration(new GridWrapItemDecoration(GridWrapItemDecoration.UIMode.SPREAD, spanCount, verticalSpace));


        //测试数据
        for (int i = 0; i < itemCount; i++) {
            items.add(i + "");
        }
        adapter.submitList(items);
        adapter2.submitList(items);
    }
}