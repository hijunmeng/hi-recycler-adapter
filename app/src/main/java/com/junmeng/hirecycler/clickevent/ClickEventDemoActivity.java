package com.junmeng.hirecycler.clickevent;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.junmeng.hirecycler.databinding.ActivityClickEventDemoBinding;
import com.junmeng.hirecycler.databinding.ItemClickTestBinding;
import com.junmeng.libadapter.base.RecyclerItemClickListener;
import com.junmeng.libadapter.viewbinding.BaseSingleViewBindingRecyclerAdapter;
import com.junmeng.libadapter.viewbinding.ViewBindingViewHolder;

import java.util.List;

/**
 * 演示了子项点击的功能，可以看到即使子view设置了监听器，在使用RecyclerItemClickListener时则可以屏蔽子view设置的点击事件
 */
public class ClickEventDemoActivity extends AppCompatActivity {

    ActivityClickEventDemoBinding binding;
    MyAdapter myAdapter;
    RecyclerView.OnItemTouchListener onItemTouchListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityClickEventDemoBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        myAdapter = new MyAdapter();
        binding.recycler.setAdapter(myAdapter);
        binding.recycler.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.HORIZONTAL));
        binding.recycler.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        for (int i = 0; i < 30; i++) {
            myAdapter.addItem(new Item());
        }
    }

    public void onClickItemDoubleClick(View view) {
        if (onItemTouchListener != null) {
            binding.recycler.removeOnItemTouchListener(onItemTouchListener);
        }

        onItemTouchListener = new RecyclerItemClickListener(binding.recycler, true) {
            @Override
            public void onItemClick(MotionEvent event, View view, int position) {
                Toast.makeText(getApplicationContext(), "item click " + position, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onItemLongClick(MotionEvent event, View view, int position) {
                Toast.makeText(getApplicationContext(), "item long click " + position, Toast.LENGTH_SHORT).show();
            }

            @Override
            public boolean onItemDoubleClick(MotionEvent event, View view, int position) {
                Toast.makeText(getApplicationContext(), "item double click " + position, Toast.LENGTH_SHORT).show();
                return true;
            }
        };
        binding.recycler.addOnItemTouchListener(onItemTouchListener);
    }

    public void onClickItemClick(View view) {
        if (onItemTouchListener != null) {
            binding.recycler.removeOnItemTouchListener(onItemTouchListener);
        }
        onItemTouchListener = new RecyclerItemClickListener(binding.recycler) {
            @Override
            public void onItemClick(MotionEvent event, View view, int position) {
                Toast.makeText(getApplicationContext(), "item click " + position, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onItemLongClick(MotionEvent event, View view, int position) {
                Toast.makeText(getApplicationContext(), "item long click " + position, Toast.LENGTH_SHORT).show();
            }
        };
        binding.recycler.addOnItemTouchListener(onItemTouchListener);
    }

    public void onClickHorizontal(View view) {
        binding.recycler.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        myAdapter.notifyDataSetChanged();
    }

    public void onClickVertical(View view) {
        binding.recycler.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        myAdapter.notifyDataSetChanged();
    }

    class MyAdapter extends BaseSingleViewBindingRecyclerAdapter<Item, ItemClickTestBinding> {

        @Override
        public void onBindView(@NonNull ViewBindingViewHolder<ItemClickTestBinding> holder, int position, Item item, @NonNull List<Object> payloads) {
            holder.binding.btn.setOnClickListener(v -> {
                Toast.makeText(holder.itemView.getContext(), "click btn " + position, Toast.LENGTH_SHORT).show();
            });
            holder.binding.btn.setOnLongClickListener(v -> {
                Toast.makeText(holder.itemView.getContext(), "long click btn " + position, Toast.LENGTH_SHORT).show();
                return true;
            });
            holder.binding.text.setText("" + position);
            holder.binding.text.setOnClickListener(v -> {
                Toast.makeText(holder.itemView.getContext(), "click text " + position, Toast.LENGTH_SHORT).show();
            });
            holder.binding.text.setOnLongClickListener(v -> {
                Toast.makeText(holder.itemView.getContext(), "long click text " + position, Toast.LENGTH_SHORT).show();
                return true;
            });
        }
    }

    class Item {
    }

}