package com.junmeng.hirecycler.common;

import android.util.Log;

import com.junmeng.hirecycler.R;
import com.junmeng.hirecycler.TestBean;
import com.junmeng.libadapter.common.BaseCommonBindView;
import com.junmeng.libadapter.common.BaseCommonRecyclerAdapter;
import com.junmeng.libadapter.common.RecyclerViewHolder;

public class BindView1 extends BaseCommonBindView<BindView1.Item> {

    @Override
    public int getItemLayoutResId(Item item) {
        return R.layout.item_one;
    }

    @Override
    public void bindViewDataWithFull(RecyclerViewHolder holder, int position, Item item) {
        Log.i("123456", "attachRecyclerView=" + holder.getAttachRecyclerView().toString());
        TestBean tb=getUserData();
        holder.setText(R.id.text, item.text+",UserData="+tb.s+",adapter="+getAdapter());
    }


    public static class Item {
        public Item(String text) {
            this.text = text;
        }

        public String text;

    }
}
