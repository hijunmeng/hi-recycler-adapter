package com.junmeng.hirecycler.common;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ConcatAdapter;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.junmeng.hirecycler.TestBean;
import com.junmeng.hirecycler.databinding.ActivityCommonDemoBinding;
import com.junmeng.libadapter.common.BaseCommonRecyclerAdapter;
import com.junmeng.libadapter.utils.RecyclerViewUtil;

import java.util.Random;

public class CommonDemoActivity extends AppCompatActivity {

    ActivityCommonDemoBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = ActivityCommonDemoBinding.inflate(getLayoutInflater());
        setContentView(mBinding.getRoot());
        BaseCommonRecyclerAdapter adapter1 = new BaseCommonRecyclerAdapter();
        adapter1.setOnItemClickLitener((v, position, o) -> {
            Toast.makeText(this, "position=" + position, Toast.LENGTH_SHORT).show();
        });
        BaseCommonRecyclerAdapter adapter2 = new BaseCommonRecyclerAdapter();
        adapter2.setOnItemClickLitener((v, position, o) -> {
            Toast.makeText(this, "position=" + position, Toast.LENGTH_SHORT).show();
        });
        mBinding.recycler.setLayoutManager(new LinearLayoutManager(this));
        //演示ConcatAdapter
        ConcatAdapter contactAdapter = new ConcatAdapter(adapter1, adapter2);
        mBinding.recycler.setAdapter(contactAdapter);
        mBinding.recycler.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        adapter1.setUserData(new TestBean("adapter1"));
        adapter2.setUserData(new TestBean("adapter2"));

        adapter1.register(new BindView1());

        adapter2.register(new BindView2());
        adapter2.register(new BindView3());

        Random r = new Random();
        for (int i = 0; i < 50; i++) {
            switch (r.nextInt(3)) {
                case 0:
                    adapter1.addItem(new BindView1.Item("i" + i));
                    break;
                case 1:
                    adapter2.addItem(new BindView2.Item("i" + i, android.R.drawable.sym_def_app_icon));
                    break;
                case 2:
                    adapter2.addItem(new BindView3.Item("i" + i, android.R.mipmap.sym_def_app_icon));
                    break;
            }
        }

        adapter1.notifyDataSetChanged();
        adapter2.notifyDataSetChanged();

    }

    public void onClickIsBottom(View view) {
        showToast(RecyclerViewUtil.isScrollToBottom(mBinding.recycler) + "");
    }

    public void onClickIsTop(View view) {
        showToast(RecyclerViewUtil.isScrollToTop(mBinding.recycler) + "");
    }

    public void onClickToBottom(View view) {
        showToast(RecyclerViewUtil.scrollToBottom(mBinding.recycler, false) + "");
    }

    public void onClickToTop(View view) {
        showToast(RecyclerViewUtil.scrollToTop(mBinding.recycler, false) + "");
    }

    public void onClickSmoothToBottom(View view) {
        showToast(RecyclerViewUtil.smoothScrollToBottom(mBinding.recycler) + "");
    }

    public void onClickSmoothToTop(View view) {
        showToast(RecyclerViewUtil.smoothScrollToTop(mBinding.recycler) + "");
    }

    public void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }
}