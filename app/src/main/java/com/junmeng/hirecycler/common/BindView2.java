package com.junmeng.hirecycler.common;

import android.util.Log;

import com.junmeng.hirecycler.R;
import com.junmeng.hirecycler.TestBean;
import com.junmeng.libadapter.common.BaseCommonBindView;
import com.junmeng.libadapter.common.RecyclerViewHolder;

public class BindView2 extends BaseCommonBindView<BindView2.Item> {

    @Override
    public int getItemLayoutResId(Item item) {
        return R.layout.item_two;
    }

    @Override
    public void bindViewDataWithFull(RecyclerViewHolder holder, int position, Item item) {
        Log.i("123456", "attachRecyclerView=" + holder.getAttachRecyclerView().toString());
        TestBean tb=getUserData();
        holder.setText(R.id.text, item.text+",UserData="+tb.s+",adapter="+getAdapter());
        holder.setImageResource(R.id.image, item.imageResId);
    }


    public static class Item {
        public String text;
        public int imageResId;

        public Item(String text, int imageResId) {
            this.text = text;
            this.imageResId = imageResId;
        }
    }
}
