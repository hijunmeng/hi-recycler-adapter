package com.junmeng.hirecycler.viewbinding;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;


import com.junmeng.hirecycler.R;
import com.junmeng.hirecycler.TestBean;
import com.junmeng.hirecycler.databinding.ActivityViewbindingDemoBinding;
import com.junmeng.libadapter.base.RecyclerItemClickListener;
import com.junmeng.libadapter.decoration.GridMatchItemDecoration;
import com.junmeng.libadapter.viewbinding.BaseViewBindingRecyclerAdapter;

import java.util.Random;

public class ViewbindingDemoActivity extends AppCompatActivity {

    ActivityViewbindingDemoBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = ActivityViewbindingDemoBinding.inflate(getLayoutInflater());
        setContentView(mBinding.getRoot());

        initListView();
        initGridView();
    }

    private void initGridView() {
        BaseViewBindingRecyclerAdapter adapter = new BaseViewBindingRecyclerAdapter();
        adapter.setOnItemClickLitener((v, position, o) -> {
            Toast.makeText(this, "position=" + position, Toast.LENGTH_SHORT).show();
        });
//        mBinding.recyclerGrid.addOnItemTouchListener(new RecyclerItemClickListener(mBinding.recyclerGrid) {
//
//            @Override
//            public void onItemClick(MotionEvent e, View view, int position) {
//                Toast.makeText(getApplicationContext(), "position=" + position, Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onItemLongClick(MotionEvent e, View view, int position) {
//                Toast.makeText(getApplicationContext(), "long position=" + position, Toast.LENGTH_SHORT).show();
//            }
//        });
        mBinding.recyclerGrid.setLayoutManager(new GridLayoutManager(this, 2));
        mBinding.recyclerGrid.setAdapter(adapter);
        mBinding.recyclerGrid.addItemDecoration(new GridMatchItemDecoration(2, 10, 10));
        adapter.setUserData(new TestBean("adapter grid"));
        adapter.register(new BindView1());
        adapter.register(new BindView2());
        adapter.register(new BindView3());

        Random r = new Random();
        for (int i = 0; i < 50; i++) {
            switch (r.nextInt(3)) {
                case 0:
                    adapter.addItem(new BindView1.Item("i" + i));
                    break;
                case 1:
                    adapter.addItem(new BindView2.Item("i" + i, android.R.drawable.sym_def_app_icon));
                    break;
                case 2:
                    adapter.addItem(new BindView3.Item("i" + i, android.R.mipmap.sym_def_app_icon));
                    break;
            }
        }
    }

    private void initListView() {
        BaseViewBindingRecyclerAdapter adapter = new BaseViewBindingRecyclerAdapter();
        adapter.setOnItemClickLitener((v, position, o) -> {
            Toast.makeText(this, "position=" + position, Toast.LENGTH_SHORT).show();
        });
        mBinding.recycler.setLayoutManager(new LinearLayoutManager(this));
        mBinding.recycler.setAdapter(adapter);
        mBinding.recycler.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        adapter.setUserData(new TestBean("adapter list"));
        adapter.register(new BindView1());
        adapter.register(new BindView2());
        adapter.register(new BindView3());

        Random r = new Random();
        for (int i = 0; i < 50; i++) {
            switch (r.nextInt(3)) {
                case 0:
                    adapter.addItem(new BindView1.Item("i" + i));
                    break;
                case 1:
                    adapter.addItem(new BindView2.Item("i" + i, R.mipmap.ic_launcher));
                    break;
                case 2:
                    adapter.addItem(new BindView3.Item("i" + i, R.mipmap.ic_launcher_round));
                    break;
            }
        }
    }
}