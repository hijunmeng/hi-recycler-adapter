package com.junmeng.hirecycler.viewbinding;

import android.util.Log;

import com.junmeng.hirecycler.TestBean;
import com.junmeng.hirecycler.databinding.ItemTwoBinding;
import com.junmeng.libadapter.viewbinding.BaseViewBindingBindView;
import com.junmeng.libadapter.viewbinding.ViewBindingViewHolder;

public class BindView2 extends BaseViewBindingBindView<BindView2.Item, ItemTwoBinding> {

    @Override
    public void bindViewDataWithFull(ViewBindingViewHolder<ItemTwoBinding> holder, int position, Item item) {
        Log.i("123456", "attachRecyclerView=" + holder.getAttachRecyclerView().toString());
        TestBean tb=getUserData();
        holder.binding.text.setText(item.text+",UserData="+tb.s+",adapter="+getAdapter());
        holder.binding.image.setImageResource(item.imageResId);
    }

    public static class Item {
        public String text;
        public int imageResId;

        public Item(String text, int imageResId) {
            this.text = text;
            this.imageResId = imageResId;
        }
    }
}
