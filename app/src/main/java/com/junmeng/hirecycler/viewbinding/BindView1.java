package com.junmeng.hirecycler.viewbinding;

import android.util.Log;

import com.junmeng.hirecycler.TestBean;
import com.junmeng.hirecycler.databinding.ItemOneBinding;
import com.junmeng.libadapter.viewbinding.BaseViewBindingBindView;
import com.junmeng.libadapter.viewbinding.ViewBindingViewHolder;

public class BindView1 extends BaseViewBindingBindView<BindView1.Item, ItemOneBinding> {

    @Override
    public void bindViewDataWithFull(ViewBindingViewHolder<ItemOneBinding> holder, int position, Item item) {
        Log.i("123456", "attachRecyclerView=" + holder.getAttachRecyclerView().toString());
        TestBean tb = getUserData();
        holder.binding.text.setText(item.text + ",UserData=" + tb.s + ",adapter=" + getAdapter());
    }

    public static class Item {
        public Item(String text) {
            this.text = text;
        }

        public String text;

    }
}