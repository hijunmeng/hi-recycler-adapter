package com.junmeng.hirecycler.viewbinding;

import android.util.Log;
import android.widget.Toast;

import com.junmeng.hirecycler.R;
import com.junmeng.hirecycler.TestBean;
import com.junmeng.hirecycler.databinding.ItemThreeBinding;
import com.junmeng.hirecycler.databinding.ItemTwoBinding;
import com.junmeng.libadapter.common.BaseCommonBindView;
import com.junmeng.libadapter.common.RecyclerViewHolder;
import com.junmeng.libadapter.viewbinding.BaseViewBindingBindView;
import com.junmeng.libadapter.viewbinding.ViewBindingViewHolder;

public class BindView3 extends BaseViewBindingBindView<BindView3.Item, ItemThreeBinding> {

    @Override
    public void bindViewDataWithFull(ViewBindingViewHolder<ItemThreeBinding> holder, int position, Item item) {
        Log.i("123456", "attachRecyclerView=" + holder.getAttachRecyclerView().toString());
        TestBean tb=getUserData();
        holder.binding.text.setText(item.text+",UserData="+tb.s+",adapter="+getAdapter());
        holder.binding.image.setImageResource(item.imageResId);
        holder.binding.button.setOnClickListener((v) -> {
            Toast.makeText(holder.itemView.getContext(), "click", Toast.LENGTH_SHORT).show();
        });
    }

    public static class Item {
        public String text;
        public int imageResId;

        public Item(String text, int imageResId) {
            this.text = text;
            this.imageResId = imageResId;
        }
    }
}
