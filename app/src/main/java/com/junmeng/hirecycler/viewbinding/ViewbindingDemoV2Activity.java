package com.junmeng.hirecycler.viewbinding;

import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.junmeng.hirecycler.TestBean;
import com.junmeng.hirecycler.databinding.ActivityViewbindingDemoBinding;
import com.junmeng.libadapter.decoration.GridMatchItemDecoration;
import com.junmeng.libadapter.v2.viewbinding.BaseViewBindingRecyclerAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ViewbindingDemoV2Activity extends AppCompatActivity {

    ActivityViewbindingDemoBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = ActivityViewbindingDemoBinding.inflate(getLayoutInflater());
        setContentView(mBinding.getRoot());

        initListView();
        initGridView();
    }

    private void initGridView() {
        BaseViewBindingRecyclerAdapter adapter = new BaseViewBindingRecyclerAdapter();
        adapter.setOnItemClickLitener((v, position, o) -> {
            Toast.makeText(this, "position=" + position, Toast.LENGTH_SHORT).show();
        });
//        mBinding.recyclerGrid.addOnItemTouchListener(new RecyclerItemClickListener(mBinding.recyclerGrid) {
//
//            @Override
//            public void onItemClick(MotionEvent e, View view, int position) {
//                Toast.makeText(getApplicationContext(), "position=" + position, Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onItemLongClick(MotionEvent e, View view, int position) {
//                Toast.makeText(getApplicationContext(), "long position=" + position, Toast.LENGTH_SHORT).show();
//            }
//        });
        mBinding.recyclerGrid.setLayoutManager(new GridLayoutManager(this, 2));
        mBinding.recyclerGrid.setAdapter(adapter);
        mBinding.recyclerGrid.addItemDecoration(new GridMatchItemDecoration(2, 10, 10));
        adapter.setUserData(new TestBean("adapter grid v2"));
        adapter.register(new BindView1());
        adapter.register(new BindView2());
        adapter.register(new BindView3());
        List datas = new ArrayList();
        Random r = new Random();
        for (int i = 0; i < 50; i++) {
            switch (r.nextInt(3)) {
                case 0:
                    datas.add(new BindView1.Item("i" + i));
                    break;
                case 1:
                    datas.add(new BindView2.Item("i" + i, android.R.drawable.sym_def_app_icon));
                    break;
                case 2:
                    datas.add(new BindView3.Item("i" + i, android.R.mipmap.sym_def_app_icon));
                    break;
            }
        }
        adapter.submitList(datas);
    }

    private void initListView() {
        BaseViewBindingRecyclerAdapter adapter = new BaseViewBindingRecyclerAdapter();
        adapter.setOnItemClickLitener((v, position, o) -> {
            Toast.makeText(this, "position=" + position, Toast.LENGTH_SHORT).show();
        });
        mBinding.recycler.setLayoutManager(new LinearLayoutManager(this));
        mBinding.recycler.setAdapter(adapter);
        mBinding.recycler.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        adapter.setUserData(new TestBean("adapter list v2"));
        adapter.register(new BindView1());
        adapter.register(new BindView2());
        adapter.register(new BindView3());
        List datas = new ArrayList();
        Random r = new Random();
        for (int i = 0; i < 50; i++) {
            switch (r.nextInt(3)) {
                case 0:
                    datas.add(new BindView1.Item("i" + i));
                    break;
                case 1:
                    datas.add(new BindView2.Item("i" + i, android.R.drawable.sym_def_app_icon));
                    break;
                case 2:
                    datas.add(new BindView3.Item("i" + i, android.R.mipmap.sym_def_app_icon));
                    break;
            }
        }
        adapter.submitList(datas);
    }
}